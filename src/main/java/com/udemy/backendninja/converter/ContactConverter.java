package com.udemy.backendninja.converter;

import org.springframework.stereotype.Component;

import com.udemy.backendninja.entity.ContactEntity;
import com.udemy.backendninja.model.Contact;

@Component("contactConverter")
public class ContactConverter {
	
	public ContactEntity convertContactToContactEntity(Contact contact){
		ContactEntity contactEntity = new ContactEntity();
		contactEntity.setId(contact.getId());
		contactEntity.setFirstname(contact.getFirstname());
		contactEntity.setLastname(contact.getLastname());
		contactEntity.setCity(contact.getCity());
		contactEntity.setTelephone(contact.getTelephone());
		return contactEntity;
	}
	
	public Contact convertContactEntityToContact(ContactEntity contactEntity){
		Contact contact = new Contact();
		contact.setId(contactEntity.getId());
		contact.setFirstname(contactEntity.getFirstname());
		contact.setLastname(contactEntity.getLastname());
		contact.setCity(contactEntity.getCity());
		contact.setTelephone(contactEntity.getTelephone());
		return contact;
	}
	
}
