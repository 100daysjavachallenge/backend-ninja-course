package com.udemy.backendninja.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.udemy.backendninja.constant.ViewConstant;
import com.udemy.backendninja.model.Contact;
import com.udemy.backendninja.service.ContactService;

@Controller
@RequestMapping("/contacts")
public class ContactController {

	@Autowired
	@Qualifier("contactServiceImpl")
	private ContactService contactService;
	
	@GetMapping("/")
	public ModelAndView index(){
		ModelAndView mav = new ModelAndView(ViewConstant.CONTACTS);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		mav.addObject("username", user.getUsername());
		mav.addObject("contacts", contactService.listAllContacts());
		return mav;
	}
	
	@PreAuthorize("hasRole('ROLE_UERu')")
	@GetMapping("/create")
	public String redirectContactForm(Model model){
		model.addAttribute("contactModel", new Contact());
		return ViewConstant.CONTACT_CREATE;
	}
	
	
	@PostMapping("store")
	public String store(@ModelAttribute(name="contactModel") Contact contactModel,
			Model model){
		if(null != contactService.addContact(contactModel)){
			model.addAttribute("result", 1);
		}else{
			model.addAttribute("result", 0);
		}
		return "redirect:/contacts/";
	}
	
	@GetMapping("/edit")
	public String edit(@RequestParam(name="id", required=true) int id,
			Model model){
		Contact contact = contactService.findContactByIdModel(id);
		
		model.addAttribute("contactModel", contact);
		return ViewConstant.CONTACT_EDIT;
	}
	
	@PostMapping("update")
	public String update(@ModelAttribute(name="contactModel") Contact contactModel,
			Model model){
		if(null != contactService.addContact(contactModel)){
			model.addAttribute("result", 1);
		}else{
			model.addAttribute("result", 0);
		}
		return "redirect:/contacts/";
	}
	
	@GetMapping("/delete")
	public ModelAndView delete(@RequestParam(name="id", required=true) int id){
		contactService.removeContact(id);
		return index();
	}
	
	@GetMapping("/cancel")
	public String cancel(){
		return "redirect:/contacts/";
	}
}
