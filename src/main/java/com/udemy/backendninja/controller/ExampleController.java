package com.udemy.backendninja.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.udemy.backendninja.component.ExampleComponent;
import com.udemy.backendninja.model.Person;
import com.udemy.backendninja.service.ExampleService;

@Controller
@RequestMapping("/example")
public class ExampleController {
	
	@Autowired   //Indica a Spring que vamos a inyectar un componente que se encuentra en su memoria
	@Qualifier("exampleComponent")//Indica a Spring el nombre del bean que esta en su memoria
	private ExampleComponent exampleComponent;
	
	@Autowired
	@Qualifier("exampleService")
	private ExampleService exampleService;//es la interfaz..no la impl
	
	//primera forma 
	//@GetMapping("/exampleString") hace lo mismo que la linea de abajo
	@RequestMapping(value="/exampleString", method=RequestMethod.GET)
	public String exampleString(Model model){
		
		exampleComponent.sayHello();
		
		model.addAttribute("people", exampleService.getListPeople());
		return "example";
	}
	
	//segunda forma
	@RequestMapping(value="/exampleMAV", method=RequestMethod.GET)
	public ModelAndView exampleMAV(){
		ModelAndView mav = new ModelAndView("example");
		mav.addObject("person", exampleService.getListPeople());
		return mav;
	}
	
	
}
