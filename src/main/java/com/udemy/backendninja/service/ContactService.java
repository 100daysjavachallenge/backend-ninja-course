package com.udemy.backendninja.service;

import java.util.List;

import com.udemy.backendninja.entity.ContactEntity;
import com.udemy.backendninja.model.Contact;

public interface ContactService {

	public abstract Contact addContact(Contact contact);
	
	public abstract List<Contact> listAllContacts();
	
	public abstract ContactEntity findContactById(int id);
	
	public abstract void removeContact(int id);
	
	public abstract Contact findContactByIdModel(int id);
	
}
