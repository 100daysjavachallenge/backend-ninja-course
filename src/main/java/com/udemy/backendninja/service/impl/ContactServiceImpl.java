package com.udemy.backendninja.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.udemy.backendninja.converter.ContactConverter;
import com.udemy.backendninja.entity.ContactEntity;
import com.udemy.backendninja.model.Contact;
import com.udemy.backendninja.repository.ContactRepository;
import com.udemy.backendninja.service.ContactService;

@Service("contactServiceImpl")
public class ContactServiceImpl implements ContactService{

	@Autowired
	@Qualifier("contactRepository")
	private ContactRepository contactRepository;
	
	@Autowired
	@Qualifier("contactConverter")
	private ContactConverter contactConverter;
	
	@Override
	public Contact addContact(Contact contact) {
		ContactEntity contactEntity = contactRepository.save(contactConverter.convertContactToContactEntity(contact));
		return contactConverter.convertContactEntityToContact(contactEntity);
	}

	@Override
	public List<Contact> listAllContacts() {
		List<ContactEntity> contactsEntity = contactRepository.findAll();
		List<Contact> contacts = new ArrayList<Contact>();
		for(ContactEntity contact : contactsEntity){
			contacts.add(contactConverter.convertContactEntityToContact(contact));
		}
		return contacts;
	}

	@Override
	public ContactEntity findContactById(int id) {
		return contactRepository.findById(id);
	}
	
	public Contact findContactByIdModel(int id){
		return contactConverter.convertContactEntityToContact(findContactById(id));
	}

	@Override
	public void removeContact(int id) {
		ContactEntity contact = findContactById(id);
		if(null != contact){
			contactRepository.delete(contact);
		}
	}

}
