package com.udemy.backendninja.model;

import javax.validation.constraints.*;

public class Person {

	@NotNull
	@Size(min=2, max=6)
	private String name;
	
	@NotNull
	@Min(18)
	private int age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Person(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	
	public Person(){}//Objeto person vacio

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + "]";
	}

	
}
