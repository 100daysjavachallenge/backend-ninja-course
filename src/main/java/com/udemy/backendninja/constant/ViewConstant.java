package com.udemy.backendninja.constant;

public class ViewConstant {

	//views
	public static final String CONTACT_CREATE = "contactform";
	public static final String CONTACT_EDIT = "contactformedit";
	public static final String CONTACTS = "contacts";
	public static final String LOGIN = "login";
	
	//User messages
	public static final String SUCCESS_MESSAGE = "El registro se ha agregado satisfactoriamente";
}
